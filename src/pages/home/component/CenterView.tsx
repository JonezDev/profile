import { useState,useRef,Ref, ReactNode, MouseEvent,useEffect } from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { Box } from '@mui/material'
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import IconButton from '@mui/material/IconButton';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import WorkHistoryOutlinedIcon from '@mui/icons-material/WorkHistoryOutlined';
import AccountTreeOutlinedIcon from '@mui/icons-material/AccountTreeOutlined';
import FlipNumbers from 'react-flip-numbers';
import moment from 'moment'
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'
import NetworkCheckIcon from '@mui/icons-material/NetworkCheck';
import WorkHistoryIcon from '@mui/icons-material/WorkHistory';

import Avatar from '@mui/material/Avatar'
import Divider from '@mui/material/Divider'
import TimelineDot from '@mui/lab/TimelineDot'
import TimelineItem from '@mui/lab/TimelineItem'
import TimelineContent from '@mui/lab/TimelineContent'
import TimelineSeparator from '@mui/lab/TimelineSeparator'
import TimelineConnector from '@mui/lab/TimelineConnector'
import MuiTimeline from '@mui/lab/Timeline'
import ReactPlayer from 'react-player'
// ** Icons Imports
import ArrowRight from 'mdi-material-ui/ArrowRight'
import MessageOutline from 'mdi-material-ui/MessageOutline'
import PhoneDialOutline from 'mdi-material-ui/PhoneDialOutline'
import PerfectScrollbarComponent, { ScrollBarProps } from 'react-perfect-scrollbar'

const PerfectScrollbar = styled(PerfectScrollbarComponent)<ScrollBarProps & { ref: Ref<unknown> }>(({ theme }) => ({
  padding: theme.spacing(0),
 
}))

const style ={
 typo:{
  fontSize:12
 },
 typoHidden:{
  fontSize:10
 },
 mainIcon:{
  fontSize:20
 },
 mainIconHidden:{
  fontSize:14
 },
 mainCardHidden:{
  justifyContent:'center',
  borderRadius:'15px',
  boxShadow:'none',
  width:'100%',
  mt:2 
 },
 mdCard:{
  justifyContent:'center',
  borderRadius:'15px',
  boxShadow:'none',
  width:'100%',
  height:'14vh',
  mt:0 
 },
 mainCard:{
  justifyContent:'center',
  borderRadius:'15px',
  boxShadow:'none',
  width:'100%',
  height:'12.7vh',
  mt:0 
 }
}
const Timeline = styled(MuiTimeline)({
  paddingLeft: 0,
  paddingRight: 0,
  '& .MuiTimelineItem-root': {
    width: '100%',
    '&:before': {
      display: 'none'
    }
  }
})

const ImgShoe = styled('img')(({ theme }) => ({
  borderRadius: theme.shape.borderRadius
}))

const TimelineLeft = () => {
  return (
    <Timeline>
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot color='error' />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
         
          <Grid container>
            <Grid item xs={8}>
            <Box sx={{ mb: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}>
            <Typography variant='body2' sx={{ mr: 2, fontWeight: 600, color: 'text.primary' }}>
              Font-End Developer
            </Typography>
          </Box>
              <Typography variant='body2' sx={{ color: 'text.primary' }}>
              <span>Charles de Gaulle Airport, Paris</span>{' '}
              <ArrowRight fontSize='small' sx={{ verticalAlign: 'bottom', mx: 4 }} />{' '}
              <span>Heathrow Airport, London</span>
            </Typography>
            <Typography variant='caption'>6:30 AM</Typography>
              <Box sx={{ mt: 2, display: 'flex', alignItems: 'center' }}>
                <img width={28} height={28} alt='invoice.pdf' src='/template/images/icons/file-icons/pdf.png' />
                <Typography variant='subtitle2' sx={{ ml: 2, fontWeight: 600 }}>
                  bookingCard.pdf
                </Typography>
              </Box>
            </Grid>
            <Grid item xs={4} >
            <Box sx={{zIndex:'tooltip',borderRadius:5,mr:3}}>
            <ReactPlayer
                // Disable download button
                muted={true}
                playing={true}
                controls={false}
                loop={true}
                
                // Disable right click
                onContextMenu={(e:any) => e.preventDefault()}

                // Your props
                url="https://www.forsightrobotics.com/wp-content/uploads/2021/11/FR_Visionaries_anim02_white.mp4"
                className="react-player"
                width="100%"
                height="100%"
              />
            </Box>
            </Grid>
          </Grid>
         
        </TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot color='primary' />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Box sx={{ mb: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}>
            <Typography variant='body2' sx={{ mr: 2, fontWeight: 600, color: 'text.primary' }}>
              Interview Schedule
            </Typography>
            <Typography variant='caption'>6th October</Typography>
          </Box>
          <Typography variant='body2' sx={{ color: 'text.primary' }}>
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Possimus quos, voluptates voluptas rem.
          </Typography>
          <Divider sx={{ my: 3 }} />
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Box sx={{ display: 'flex' }}>
              <Avatar src='/template/images/avatars/2.png' sx={{ width: '2rem', height: '2rem', mr: 2 }} />
              <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                <Typography variant='body2' sx={{ fontWeight: 600 }}>
                  Rebecca Godman
                </Typography>
                <Typography variant='caption'>Javascript Developer</Typography>
              </Box>
            </Box>
            <div>
              <IconButton sx={{ color: 'text.secondary' }}>
                <MessageOutline fontSize='small' />
              </IconButton>
              <IconButton sx={{ color: 'text.secondary' }}>
                <PhoneDialOutline fontSize='small' />
              </IconButton>
            </div>
          </Box>
        </TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot color='warning' />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Box sx={{ mb: 3, display: 'flex', flexDirection: { sm: 'row', xs: 'column' } }}>
            <ImgShoe width='85' height='85' alt='Shoe img' src='/template/images/misc/shoe.jpeg' />
            <Box sx={{ ml: { sm: 3, xs: 0 } }}>
              <Box
                sx={{ mb: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}
              >
                <Typography
                  variant='body2'
                  sx={{ mr: 2, fontWeight: 600, color: 'text.primary', mt: { sm: 0, xs: 2 } }}
                >
                  Sold Puma POPX Blue Color
                </Typography>
                <Typography
                  variant='caption'
                  sx={{
                    mb: {
                      sm: 0,
                      xs: 2
                    }
                  }}
                >
                  4th October
                </Typography>
              </Box>
              <Typography variant='body2' sx={{ mb: 2, color: 'text.primary' }}>
                PUMA presents the latest shoes from its collection. Light & comfortable made with highly durable
                material.
              </Typography>
            </Box>
          </Box>
          <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-between', textAlign: 'center' }}>
            <Box sx={{ mr: 2 }}>
              <Typography variant='subtitle2' sx={{ fontWeight: 600 }}>
                Customer
              </Typography>
              <Typography variant='caption'>Micheal Scott</Typography>
            </Box>
            <Box sx={{ mr: 2 }}>
              <Typography variant='subtitle2' sx={{ fontWeight: 600 }}>
                Price
              </Typography>
              <Typography variant='caption'>375.00</Typography>
            </Box>
            <Box>
              <Typography variant='subtitle2' sx={{ fontWeight: 600 }}>
                Quantity
              </Typography>
              <Typography variant='caption'>1</Typography>
            </Box>
          </Box>
        </TimelineContent>
      </TimelineItem>

      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot color='success' />
          <TimelineConnector />
        </TimelineSeparator>
        <TimelineContent>
          <Box sx={{ mb: 2, display: 'flex', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between' }}>
            <Typography variant='body2' sx={{ mr: 2, fontWeight: 600, color: 'text.primary' }}>
              Design Review
            </Typography>
            <Typography variant='caption'>4th October</Typography>
          </Box>
          <Typography variant='body2' sx={{ mb: 2, color: 'text.primary' }}>
            Weekly review of freshly prepared design for our new application.
          </Typography>
          <Box sx={{ display: 'flex', alignItems: 'center' }}>
            <Avatar src='/template/images/avatars/1.png' sx={{ width: '2rem', height: '2rem', mr: 2 }} />
            <Typography variant='subtitle2' sx={{ fontWeight: 600 }}>
              John Doe (Client)
            </Typography>
          </Box>
        </TimelineContent>
      </TimelineItem>
    </Timeline>
  )
}

const CenterView = () => {
  const theme = useTheme()
  const chatArea = useRef(null)
  const hiddensm = useMediaQuery(theme.breakpoints.only('xs'))
  const hiddenMd = useMediaQuery(theme.breakpoints.only('md'))

  const hidden = useMediaQuery(theme.breakpoints.down('md'))
  const showList = [
    {
      shortName:'EXP',
      longName:'Work Experience',
      value:"6",
      unit:'mount',
      unitShort:'mount',
      icon:<WorkHistoryOutlinedIcon style={hidden?style.mainIconHidden:style.mainIcon}/>
    },
    {
      shortName:'PJ',
      longName:'Project Done',
      value:"3",
      unit:'EA',
      unitShort:'EA',
      icon:<AccountTreeOutlinedIcon style={hidden?style.mainIconHidden:style.mainIcon}/>
    },
    {
      shortName:'JS',
      longName:'Job Success',
      value:"94",
      unit:'%',
      unitShort:'%',
      icon:<NetworkCheckIcon style={hidden?style.mainIconHidden:style.mainIcon}/>
    },
    {
      shortName:'AVBL',
      longName:'Availability',
      value:"40",
      unit:'hrs/week',
      unitShort:'h/w',
      icon:<WorkHistoryIcon style={hidden?style.mainIconHidden:style.mainIcon}/>
    },
  ]
  const ScrollWrapper = ({ children }: { children: ReactNode }) => {
  return (
          <PerfectScrollbar ref={chatArea} options={{ wheelPropagation: false }}>
                {children}
          </PerfectScrollbar>
  )
}

  const ShowItemList = (row:any) => {
    return(
        <Grid container key={row.shortName}  item xs={3}>
          <Grid  container item justifyContent={'center'} xs={3}>
            {row.icon}
          </Grid>

          <Grid item container justifyContent={hidden?'center':'flex-start'} alignItems='center' xs={7}>
            <Typography style={hidden?style.typoHidden:style.typo}> {hidden ?row.shortName :hiddenMd?row.shortName:row.longName} </Typography>
          </Grid>

          <Grid item   xs={2.5}>
          </Grid>

          <Grid container justifyContent={'center'} item xs={hidden?5:4} >
            <Typography variant={hidden?'h4' :'h3'} >{row.value}</Typography>
          </Grid  >

          <Grid item container xs={4}  justifyContent={'center'} alignItems='flex-end'>
            <Typography style={hidden?style.typoHidden:style.typo} >{hidden?row.unitShort:hiddenMd?row.unitShort:row.unit}</Typography>
          </Grid>

        </Grid>
    )
  }

  return (
        <>
          <Grid container spacing={hidden?2:5} >
            <Grid item container xs={12} justifyContent={'center'} >
            <Card sx={hiddensm?style.mainCardHidden:hiddenMd?style.mdCard:style.mainCard} >
              
              <CardContent style={hidden?{padding:'10px'}:{padding:'20px'}}>
                <Grid container spacing={2} >
            
                  {showList.map((item:any,index) => ShowItemList(item))}

                  
                </Grid>
                </CardContent>   
              </Card>  
            </Grid>

           
            <Grid item container xs={12} justifyContent={'center'} >  

              {hidden
              ?  <Card sx={{boxShadow:'none',borderRadius:'15px',height:'100%',width:'100%'}} >
                  <CardContent >
                
                   <b>WORK EXPERIENCE</b>

                      
                    </CardContent>   
                  </Card>
              : <Card sx={{boxShadow:'none',borderRadius:'15px',height:'100%',width:'100%'}} >
                  <CardContent >
                
                    <b>WORK EXPERIENCE</b>
                     <ScrollWrapper>
                      <Box sx={{ height: '65vh'}}>
                        <TimelineLeft/>
                      </Box>
                    </ScrollWrapper>
                    </CardContent>   
                  </Card>
              }
            </Grid>  
            </Grid>
        </>
        
     
  )
}

export default CenterView
