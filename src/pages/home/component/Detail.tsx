import { useState,useRef,Ref, ReactNode, MouseEvent,useEffect } from 'react'

// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { Box } from '@mui/material'
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import IconButton from '@mui/material/IconButton';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'
import RoomSharpIcon from '@mui/icons-material/RoomSharp';
import LocalPhoneSharpIcon from '@mui/icons-material/LocalPhoneSharp';
import LocalPostOfficeSharpIcon from '@mui/icons-material/LocalPostOfficeSharp';
import MoreVertSharpIcon from '@mui/icons-material/MoreVertSharp';
import PerfectScrollbarComponent, { ScrollBarProps } from 'react-perfect-scrollbar'

const PerfectScrollbar = styled(PerfectScrollbarComponent)<ScrollBarProps & { ref: Ref<unknown> }>(({ theme }) => ({
  padding: theme.spacing(0),
 
}))

const style ={
  NavButton:{
    height:40,
    width:40,
    padding:'1px'
      
  },
  NavIconButton:{
    fontSize:28,
    '&:hover': {
      fontSize:38,
     
    }
  },
  iconContact:{
    fontSize:14,
    marginRight:'5px',
    marginBottom:'5px'
  },
  imgSkill:{
    width:'40px',
    height:'40px',
    marginLeft:'10px',
    marginRight:'20px',
    borderRadius:'10%'
  }
}

const Detail = () => {
  const theme = useTheme()
  const hidden = useMediaQuery(theme.breakpoints.down('md'))
  const hiddenlg = useMediaQuery(theme.breakpoints.only('md'))

  const chatArea = useRef(null)

  const skillList = [
    {
      title:'Javascript',
      value:9,
      icon:<img src={'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png'} style={style.imgSkill} />

    },
    {
      title:'typescript',
      value:8,
      icon:<img src={'https://www.svgrepo.com/show/303600/typescript-logo.svg'} style={style.imgSkill} />
    },
    {
      title:'React',
      value:9,
      icon:<img src={'https://pbs.twimg.com/media/E0ks4yBWYBQaVDV?format=png&name=small'} style={style.imgSkill} />
    },
    {
      title:'Material UI',
      value:9,
      icon:<img src={'https://img.icons8.com/color/480/material-ui.png'} style={style.imgSkill} />

    },
    
    {
      title:'Redux',
      value:8,
      icon:<img src={'https://e7.pngegg.com/pngimages/522/1018/png-clipart-purple-atom-redux-logo-icons-logos-emojis-tech-companies-thumbnail.png'} style={style.imgSkill} />

    },
    {
      title:'Next',
      value:8,
      icon:<img src={'https://seeklogo.com/images/N/next-js-logo-8FCFF51DD2-seeklogo.com.png'} style={style.imgSkill} />
    },
    {
      title:'Git',
      value:7,
      icon:<img src={'https://git-scm.com/images/logos/downloads/Git-Icon-1788C.png'} style={style.imgSkill} />
    },
    {
      title:'Postman',
      value:8,
      icon:<img src={'https://seeklogo.com/images/P/postman-logo-0087CA0D15-seeklogo.com.png'} style={style.imgSkill} />
    },
    {
      title:'FileZilla',
      value:7,
      icon:<img src={'https://upload.wikimedia.org/wikipedia/commons/thumb/0/01/FileZilla_logo.svg/1200px-FileZilla_logo.svg.png'} style={style.imgSkill} />
    },
    {
      title:'SQL',
      value:7,
      icon:<img src={'https://www.freeiconspng.com/thumbs/sql-server-icon-png/sql-server-icon-png-29.png'} style={style.imgSkill} />
    },
    {
      title:'firebase',
      value:6,
      icon:<img src={'https://miro.medium.com/max/300/1*R4c8lHBHuH5qyqOtZb3h-w.png'} style={style.imgSkill} />
    },
    {
      title:'Adobe Xd',
      value:6,
      icon:<img src={'https://upload.wikimedia.org/wikipedia/commons/thumb/c/c2/Adobe_XD_CC_icon.svg/2101px-Adobe_XD_CC_icon.svg.png'} style={style.imgSkill} />

    },
    {
      title:'HTML',
      value:7,
      icon:<img src={'https://cdn-icons-png.flaticon.com/512/1532/1532556.png'} style={style.imgSkill} />
    },
    {
      title:'CSS',
      value:7,
      icon:<img src={'https://seeklogo.com/images/C/css-3-logo-023C1A7171-seeklogo.com.png'} style={style.imgSkill} />

    },
    
  
  ]
  const levelSkill = (value:any) => {
    let level = []
    for (let i =0; i < 10; i++) {
      level.push(<span style={{color:'#FFD700'}}>&#9733;</span>)
    }
    
    return level
  
  }
  const ScrollWrapper = ({ children }: { children: ReactNode }) => {
    return (
            <PerfectScrollbar ref={chatArea} options={{ wheelPropagation: false }}>
                 {children}
            </PerfectScrollbar>
    )
  }
  return (
        <>
             <Grid item  xs={12}  sx={{mt:hidden ?2:5}} container justifyContent={'flex-start'} >
              <Card sx={{boxShadow:'',borderRadius:'15px',height:'100%',width:'100%'}}>
                <CardContent>
                 </CardContent>
              </Card>
            </Grid>
            
            <Grid item  xs={12}  sx={{mt:hidden ?2:5}} container justifyContent={'flex-start'} >
              <Card sx={{boxShadow:'none',borderRadius:'15px',height:'100%',width:'100%'}}>
                <CardContent>
                  <b>SKILL</b>
                  {hidden
                    ?<>
                    {skillList.map((item,index)=>(
                        <Card key={index} sx={{my:3,boxShadow:'none',bgcolor:'#F5F8FD'}}>
                          <CardContent>
                            <Grid sx={{display:'flex',alignItems:'center'}} >
                              <MoreVertSharpIcon 
                              sx={{color:'#8B888F'}}/>
                              {item.icon}
                              <Box>
                                <b>{item.title}</b><br/>
                                <Grid container>
                                  
                                    {levelSkill(item.value).map((list,i)=>
                                    <Box key={i} sx={{bgcolor:i+1 <= item.value?'#FE5990':'#FEE1EB',width:'1vh',height:'1vh',mr:1}}>
                                    </Box>)}
                                  
                                  
                                </Grid>
                              </Box>
                            </Grid>
                          </CardContent>
                        </Card>
                      ))}
                    </>
                    :<>
                    <Box  sx={{ height: '50vh'}}>
                    <ScrollWrapper>
                      {skillList.map((item,index)=>(
                        <Card key={index} sx={{my:3,boxShadow:'none',bgcolor:'#F5F8FD'}}>
                          <CardContent>
                            <Grid sx={{display:'flex',alignItems:'center'}} >
                              <MoreVertSharpIcon 
                              sx={{color:'#8B888F'}}/>
                              {item.icon}
                              <Box>
                                <b>{item.title}</b><br/>
                                <Grid container>
                                  
                                    {levelSkill(item.value).map((list,i)=>
                                    <Box key={i} sx={{bgcolor:i+1 <= item.value?'#FE5990':'#FEE1EB',width:'1vh',height:'1vh',mr:1}}>
                                    </Box>)}
                                  
                                  
                                </Grid>
                              </Box>
                            </Grid>
                          </CardContent>
                        </Card>
                      ))}
                    </ScrollWrapper>
                  </Box>
                    </>
                  }
                  
                 
                </CardContent>
              </Card>
            </Grid>

        </>
        
     
  )
}

export default Detail
