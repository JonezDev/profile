// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { Box } from '@mui/material'
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import IconButton from '@mui/material/IconButton';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'
import RoomSharpIcon from '@mui/icons-material/RoomSharp';
import LocalPhoneSharpIcon from '@mui/icons-material/LocalPhoneSharp';
import LocalPostOfficeSharpIcon from '@mui/icons-material/LocalPostOfficeSharp';
import MoreVertSharpIcon from '@mui/icons-material/MoreVertSharp';

const style ={
  NavButton:{
    height:40,
    width:40,
    padding:'1px'
      
  },
  NavIconButton:{
    fontSize:28,
    '&:hover': {
      fontSize:38,
     
    }
  },
  iconContact:{
    fontSize:14,
    marginRight:'5px',
    marginBottom:'5px'
  },
  imgSkill:{
    width:'40px',
    height:'40px',
    marginLeft:'10px',
    marginRight:'20px',
    borderRadius:'10%'
  }
}

const Detail = () => {
  const theme = useTheme()
  const hidden = useMediaQuery(theme.breakpoints.down('lg'))
  const hiddenlg = useMediaQuery(theme.breakpoints.only('md'))

  const skillList = [
    {
      title:'Javascript',
      value:9,
      icon:<img src={'https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/480px-Unofficial_JavaScript_logo_2.svg.png'} style={style.imgSkill} />

    },
    {
      title:'typescript',
      value:8,
      icon:<img src={'https://www.svgrepo.com/show/303600/typescript-logo.svg'} style={style.imgSkill} />
    },
    {
      title:'React',
      value:8,
      icon:<img src={'https://pbs.twimg.com/media/E0ks4yBWYBQaVDV?format=png&name=small'} style={style.imgSkill} />
    },
    {
      title:'Next',
      value:6,
      icon:<img src={'https://seeklogo.com/images/N/next-js-logo-8FCFF51DD2-seeklogo.com.png'} style={style.imgSkill} />
    },
    {
      title:'Material UI',
      value:8,
      icon:<img src={'https://img.icons8.com/color/480/material-ui.png'} style={style.imgSkill} />

    },
    {
      title:'HTML',
      value:6,
      icon:<img src={'https://cdn-icons-png.flaticon.com/512/1532/1532556.png'} style={style.imgSkill} />

    },
    {
      title:'CSS',
      value:8,
      icon:<img src={'https://seeklogo.com/images/C/css-3-logo-023C1A7171-seeklogo.com.png'} style={style.imgSkill} />

    },
  
  ]
  const levelSkill = (value:any) => {
    let level = []
    for (let i = 0; i < 10; i++) {
      level.push(<span style={{color:'#FFD700'}}>&#9733;</span>)
    }
  return level
  
  }
  const HeadBio = ()=>{
    return(
      <Box>
         <Card sx={{zIndex: 'tooltip',position: 'absolute',height:'10vh',borderRadius:'50%',width:'10vh',mb:2,mt:3,ml:2 ,boxShadow:'none',border:5,borderColor:(theme)=> theme.palette.customColors.lightBg }}  >
      <CardContent >
        qweq         
      </CardContent>   
    </Card>
      <Card  sx={{zIndex: 'modal',height:'6vh',position: 'static ',borderRadius:'15px',mb:1.5 ,boxShadow:'none',borderBottomLeftRadius:0,borderBottomRightRadius:0 }} >
      <CardContent  sx={hidden?{pl:27,fontSize:14}:{pl:30}} style={hidden?{paddingTop:'15px',paddingBottom:'15px'}:{paddingTop:'30px',paddingBottom:'10px'}}>
        <b>NATTHAPONG PHETKHAI</b>
      </CardContent>   
    </Card>
   
    <Card  sx={{zIndex: 'modal',height:hidden?'11vh':'vh',position: 'static ',borderRadius:'15px' ,boxShadow:'none',borderTopLeftRadius:0,borderTopRightRadius:0}} >
              <CardContent sx={hidden?{pl:27}:{pl:30}} style={{fontSize:9,paddingTop:'11px',paddingBottom:'10px'}}>
                {!hidden
                ?<>
                  <Box sx={{display:'flex',alignContent:'center'}}>
                    <LocalPhoneSharpIcon style={style.iconContact} /> 094-973-5505
                    <LocalPostOfficeSharpIcon style={style.iconContact} sx={{ml:2}} /> natthapong.phetkhai@gmail.com
                  </Box>
                  <Box sx={{display:'flex'}}>
                  </Box>
                  <Box sx={{display:'flex'}}>
                    <RoomSharpIcon style={style.iconContact} /> 65000 Dontong, Phitsanulok ,Thailand
                  </Box>
                 </>
                :<>
                <Box sx={{display:'flex',alignContent:'center'}}>
                  <LocalPhoneSharpIcon style={style.iconContact} /> 094-973-5505
                </Box>
                <Box sx={{display:'flex'}}>
                  <LocalPostOfficeSharpIcon style={style.iconContact}  /> natthapong.phetkhai@gmail.com
                </Box>
                <Box sx={{display:'flex'}}>
                  <RoomSharpIcon style={style.iconContact} /> 65000 Dontong, Phitsanulok ,Thailand
                </Box>
               </>
                }
                
                
              </CardContent>   
    </Card>
    </Box>
    )
  }
  return (
        <>
           
             <HeadBio/>
              
            {/* <Grid  xs={12}  sx={{zIndex: 'modal'}} container justifyContent={'flex-start'} >
              <Card  sx={{height:'100%',width:'100%',borderRadius:'15px',mb:1.5 ,boxShadow:'none',borderBottomLeftRadius:0,borderBottomRightRadius:0 }} >
              <CardContent  sx={hidden?{pl:27}:{pl:30}} style={hidden?{paddingTop:'15px',paddingBottom:'15px'}:{paddingTop:'30px',paddingBottom:'10px'}}>
                <b>NATTHAPONG PHETKHAI</b>
              </CardContent>   
            </Card>
            </Grid>
            <Grid  xs={12} md={4}  justifyContent={'center'} sx={{position: 'relative',zIndex: 'tooltip'}}>
            <Card sx={{height:'10vh',borderRadius:'50%',width:'10vh',mb:2,mt:3,ml:2 ,boxShadow:'none',border:5,borderColor:(theme)=> theme.palette.customColors.lightBg }}  >
              <CardContent >
                qweq         
              </CardContent>   
            </Card>
            </Grid>
            <Grid  xs={12} sx={{zIndex: 'modal'}} container justifyContent={'center'} >
              <Card  sx={{height:'100%',width:'100%',borderRadius:'15px' ,boxShadow:'none',borderTopLeftRadius:0,borderTopRightRadius:0}} >
              <CardContent sx={hidden?{pl:27}:{pl:30}} style={{fontSize:9,paddingTop:'15px',paddingBottom:'10px'}}>
                {!hidden
                ?<>
                  <Box sx={{display:'flex',alignContent:'center'}}>
                    <LocalPhoneSharpIcon style={style.iconContact} /> 094-973-5505
                    <LocalPostOfficeSharpIcon style={style.iconContact} sx={{ml:2}} /> natthapong.phetkhai@gmail.com
                  </Box>
                  <Box sx={{display:'flex'}}>
                  </Box>
                  <Box sx={{display:'flex'}}>
                    <RoomSharpIcon style={style.iconContact} /> 65000 Dontong, Phitsanulok ,Thailand
                  </Box>
                 </>
                :<>
                <Box sx={{display:'flex',alignContent:'center'}}>
                  <LocalPhoneSharpIcon style={style.iconContact} /> 094-973-5505
                </Box>
                <Box sx={{display:'flex'}}>
                  <LocalPostOfficeSharpIcon style={style.iconContact}  /> natthapong.phetkhai@gmail.com
                </Box>
                <Box sx={{display:'flex'}}>
                  <RoomSharpIcon style={style.iconContact} /> 65000 Dontong, Phitsanulok ,Thailand
                </Box>
               </>
                }
                
                
              </CardContent>   
            </Card>
            </Grid> */}

        </>
        
     
  )
}

export default Detail
