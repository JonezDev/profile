import { useState } from 'react'
// ** MUI Imports
import Card from '@mui/material/Card'
import Grid from '@mui/material/Grid'
import Typography from '@mui/material/Typography'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import BlankLayout from 'src/@core/layouts/BlankLayout'
import { Box } from '@mui/material'
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import IconButton from '@mui/material/IconButton';
import WorkOutlineOutlinedIcon from '@mui/icons-material/WorkOutlineOutlined';
import Detail from './component/Detail'
import CenterView from './component/CenterView'
import { Doughnut } from 'react-chartjs-2';
import useMediaQuery from '@mui/material/useMediaQuery'
import { styled, useTheme } from '@mui/material/styles'
import HeadBio from './component/HeadBio'



const Home = () => {
  const [navButton,setNavButton] = useState('Home')
  const theme = useTheme()
  const hidden = useMediaQuery(theme.breakpoints.down('md'))
  const hiddenlg = useMediaQuery(theme.breakpoints.only('md'))

  const style ={
    NavButton:{
      height:hidden ?20 :40,
      width:hidden ?20 :40,
      padding:'10px',
      
    },
    NavIconButton:{
      fontSize:28,
      
      '&:hover': {
        fontSize:38,
        
       
      },
     

    }
  }
  const navButtonData = [
    {
      name:'Home',
      icon:<HomeOutlinedIcon sx={style.NavIconButton}/>
    },
    {
      name:'Resume',
      icon:<AccountCircleOutlinedIcon sx={style.NavIconButton}/>
    },
    {
      name:'Work',
      icon:<WorkOutlineOutlinedIcon sx={style.NavIconButton}/>
    },
  ]

  const NavButtonList =(row:any)=>{

    return(
            <Grid item  sx={{mb:1}} >
              <IconButton  color="secondary" key={row.name} style={navButton == row.name ?{backgroundColor:'#DFF4FE',color:'#07AEF4'}:{}}  onClick={()=>{setNavButton(row.name)}} sx={style.NavButton}>
                {row.icon}
                </IconButton>
             </Grid>  
    )
  }
  
  return (
      <Grid  container  justifyContent='center' sx={{pt:hidden ?2 : 5,pb:hidden ?2 : 10,height:hidden ?'auto':'100vh'}}>
      <Grid item container={!hidden} xs={11} spacing={5} >
      <Grid item xs={hidden ?12: 0.6} sx={{minWidth:'80px'}} >
        <Card  sx={{height:'100%' ,borderRadius:'15px',boxShadow:'none'}}>
          <CardContent  style={{padding:'10px'}} >
            <Grid container spacing={hidden?10:2} justifyContent={'center'} >
              {navButtonData.map((row:any)=>NavButtonList(row))}
            </Grid>
          </CardContent>
        </Card>
      </Grid>
      <Grid item xs={12} md={7}  >
        <CenterView />
      </Grid>
      <Grid item xs={12} md={3.99} lg={4} sx={hidden ?{pt:2}:{}}  >
        <HeadBio/>
         <Detail/>

      </Grid>
      </Grid>
     
    </Grid>
  )
}
Home.getLayout = (page: any) => <BlankLayout>{page}</BlankLayout>
Home.guestGuard = true
export default Home
